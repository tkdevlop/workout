const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateProfileInput(data) {
  let errors = {};

  data.status = !isEmpty(data.status) ? data.status : "";
  data.BMI = !isEmpty(data.BMI) ? data.BMI : "";

  if (Validator.isEmpty(data.status)) {
    errors.status = "Status field is required";
  }
  if (Validator.isEmpty(data.BMI)) {
    errors.BMI = "BMI field is required";
  }
  if (!Validator.isLength(data.bio, { min: 10, max: 100 })) {
    errors.bio = "about needs to between 10 and 100 characters";
  }
  if (Validator.isEmpty(data.bio)) {
    errors.bio = "Bio field is required";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
