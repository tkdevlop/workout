// const express = require("express");
// const mongoose = require("mongoose");
// const bodyParser = require("body-parser");
// const passport = require("passport");
// const users = require("./routes/api/users");
// const profile = require("./routes/api/profile");
// const instructor = require("./routes/api/instructor");
// const fs = require("fs");
// const path = require("path");
// var ss = require("socket.io-stream");
// const app = express();
// const
// const sockets = [];
// var http = require("http").Server(app);
// var io = require("socket.io")(http);
// // Body parser middleware
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

// // DB Config
// const db = require("./config/keys").mongoURI;

// // Connect to MongoDB
// mongoose
//   .connect(
//     db,
//     { useNewUrlParser: true }
//   )
//   .then(() => console.log("MongoDB Connected"))
//   .catch(err => console.log(err));
// app.use(passport.initialize());
// require("./config/passport")(passport);

// app.use("/api/users", users);
// app.use("/api/instructors", instructor);
// app.use("/api/profile", profile);

// // static
// app.use(express.static(__dirname + "/client/build"));

// app.get("*", (req, res) => {
//   res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
// });

// // io.on("connection", function(socket) {
// //   socket.emit("news", { hello: "world" });

// //   socket.on("livestream", function(id, msg) {
// //     console.log("test");
// //   });

// //   socket.on("disconnect", function() {
// //     io.emit("user disconnected");
// //   });
// // });

// // io.on("connection", function(socket) {
// //   socket.on("stream", function(image) {
// //     socket.broadcast.emit("stream", image);
// //   });
// // });

// var numUsers = 0;

// // io.on("connection", socket => {
// //   var addedUser = false;
// //   const buffer = [];
// //   socket.on("blobs", function(stream, data) {
// //     socket.broadcast.emit("blobs", stream);
// //     buffer.push(stream);
// //   });
// //   socket.on("disconnect", () => {
// //     // var wstream = fs.createWriteStream("myBinaryFile.webm");

// //     // wstream.write(buffer.join(" "));
// //     // wstream.end();
// //     // console.log(buffer);
// //     fs.writeFile("test.mp4", buffer.join(" "), "binary", function(err) {
// //       if (err) {
// //         console.log(err);
// //       } else {
// //         console.log("The file was saved!");
// //       }
// //     });
// //   });
// //   ss(socket).on("profile-image", function(stream, data) {
// //     var filename = path.basename(data.name);
// //     stream.pipe(fs.createWriteStream(filename));
// //   });

// //   // when the client emits 'new message', this listens and executes
// //   socket.on("new message", data => {
// //     // we tell the client to execute 'new message'
// //     socket.broadcast.emit("new message", {
// //       username: socket.username,
// //       message: data
// //     });
// //   });

// //   // when the client emits 'add user', this listens and executes
// //   socket.on("add user", username => {
// //     if (addedUser) return;

// //     // we store the username in the socket session for this client
// //     socket.username = username;
// //     ++numUsers;
// //     addedUser = true;
// //     socket.emit("login", {
// //       numUsers: numUsers
// //     });
// //     // echo globally (all clients) that a person has connected
// //     socket.broadcast.emit("user joined", {
// //       username: socket.username,
// //       numUsers: numUsers
// //     });
// //   });

// //   // when the client emits 'typing', we broadcast it to others
// //   socket.on("typing", () => {
// //     socket.broadcast.emit("typing", {
// //       username: socket.username
// //     });
// //   });

// //   // when the client emits 'stop typing', we broadcast it to others
// //   socket.on("stop typing", () => {
// //     socket.broadcast.emit("stop typing", {
// //       username: socket.username
// //     });
// //   });

// //   // when the user disconnects.. perform this
// //   // socket.on("disconnect", () => {
// //   //   if (addedUser) {
// //   //     --numUsers;

// //   //     // echo globally that this client has left
// //   //     socket.broadcast.emit("user left", {
// //   //       username: socket.username,
// //   //       numUsers: numUsers
// //   //     });
// //   //   }
// //   // });
// // });

// io.on("connection", function(socket) {
//   socket.on("message", function(data) {
//     console.log("writing to disk");
//     writeToDisk(data.audio.dataURL, data.audio.name);
//     writeToDisk(data.video.dataURL, data.video.name);

//     merge(socket, data.audio.name, data.video.name);
//   });
//   socket.emit("add-users", {
//     users: sockets
//   });

//   socket.broadcast.emit("add-users", {
//     users: [socket.id]
//   });
//   socket.broadcast.on("blobs", data => {
//     socket.broadcast.emit("blobs", data);
//   });
//   socket.on("make-offer", function(data) {
//     console.log(data);
//     socket.to(data.to).emit("offer-made", {
//       offer: data.offer,
//       socket: socket.id
//     });
//   });

//   socket.on("make-connection", function(data) {
//     socket.to(data.to).emit("offer-made", {
//       offer: data.offer
//     });
//   });

//   socket.on("make-answer", function(data) {
//     socket.to(data.to).emit("answer-made", {
//       socket: socket.id,
//       answer: data.answer
//     });
//   });

//   socket.on("disconnect", function() {
//     sockets.splice(sockets.indexOf(socket.id), 1);
//     io.emit("remove-user", socket.id);
//   });

//   sockets.push(socket.id);
// });
// const port = process.env.PORT || 5000;

// http.listen(port, () => console.log(`Server running on port ${port}`));

// Last time updated at June 02, 2017

// Muaz Khan      - www.MuazKhan.com
// MIT License    - www.WebRTC-Experiment.com/licence
// RecordRTC      - github.com/muaz-khan/RecordRTC

// RecordRTC over Socket.io - https://github.com/muaz-khan/RecordRTC/tree/master/RecordRTC-over-Socketio
var http = require("http"),
  url = require("url"),
  path = require("path"),
  fs = require("fs"),
  uuid = require("node-uuid"),
  child_process = require("child_process"),
  port = process.argv[2] || 9001,
  util = require("util");
console.log("http://localhost:" + port);

var app = http
  .createServer(function(request, response) {
    var uri = url.parse(request.url).pathname,
      filename = path.join(process.cwd(), uri);

    fs.exists(filename, function(exists) {
      if (!exists) {
        response.writeHead(404, {
          "Content-Type": "text/plain"
        });
        response.write("404 Not Found: " + filename + "\n");
        response.end();
        return;
      }

      if (fs.statSync(filename).isDirectory())
        filename += "/client/build/index.html";

      fs.readFile(filename, "binary", function(err, file) {
        if (err) {
          response.writeHead(500, {
            "Content-Type": "text/plain"
          });
          response.write(err + "\n");
          response.end();
          return;
        }

        response.writeHead(200);
        response.write(file, "binary");
        response.end();
      });
    });
  })
  .listen(parseInt(port, 10));

var path = require("path"),
  exec = require("child_process").exec;

var io = require("socket.io").listen(app);

io.sockets.on("connection", function(socket) {
  socket.on("blobs", blobs => {
    socket.broadcast.emit("blobs", blobs);
  });
  socket.on("message", function(data) {
    var fileName = uuid.v4();

    socket.emit("ffmpeg-output", 0);

    writeToDisk(data.audio.dataURL, fileName + ".wav");

    // if it is chrome
    if (data.video) {
      console.log("MERGED");
      // writeToDisk(data.video.dataURL, fileName + ".webm");
      merge(socket, fileName);
    }

    // if it is firefox or if u ser is recording only audio
    else socket.emit("merged", fileName + ".wav");
  });
});

// isn't it redundant?
// app.listen(8888);

function writeToDisk(dataURL, fileName) {
  var fileExtension = fileName.split(".").pop(),
    fileRootNameWithBase = "./uploads/" + fileName,
    filePath = fileRootNameWithBase,
    fileID = 2,
    fileBuffer;

  // @todo return the new filename to client
  while (fs.existsSync(filePath)) {
    filePath = fileRootNameWithBase + "(" + fileID + ")." + fileExtension;
    fileID += 1;
  }

  dataURL = dataURL.split(",").pop();
  fileBuffer = new Buffer(dataURL, "base64");
  fs.writeFileSync(filePath, fileBuffer);

  console.log("filePath", filePath);
}

function merge(socket, fileName) {
  var FFmpeg = require("fluent-ffmpeg");

  var audioFile = path.join(__dirname, "uploads", fileName + ".wav"),
    videoFile = path.join(__dirname, "uploads", fileName + ".webm"),
    mergedFile = path.join(__dirname, "uploads", fileName + "-merged.webm");

  // new FFmpeg({
  //   source: videoFile
  // })
  //   .addInput(audioFile)
  //   .on("error", function(err) {
  //     socket.emit("ffmpeg-error", "ffmpeg : An error occurred: " + err.message);
  //   })
  //   .on("progress", function(progress) {
  //     socket.emit("ffmpeg-output", Math.round(progress.percent));
  //   })
  //   .on("end", function() {
  //     socket.emit("merged", fileName + "-merged.webm");
  //     console.log("Merging finished !");

  //     // removing audio/video files
  //     fs.unlink(audioFile);
  //     fs.unlink(videoFile);
  //   })
  //   .saveToFile(mergedFile);
  var exec = child_process.exec;

  function puts(error, stdout, stderr) {
    stdout ? console.log("stdout: " + stdout) : null;
    stderr ? console.log("stderr: " + stderr) : null;
    error ? console.log("exec error: " + error) : null;
    fs.unlinkSync(videoFile);
    fs.unlinkSync(audioFile);
  }

  exec(
    `ffmpeg -i ${videoFile} -i ${audioFile} -map 0:0 -map 1:0 ${mergedFile}`,
    puts
  );
}
